<?php

use Illuminate\Http\Request;

app('Route')::middleware('api')->post(
    '/login',
    'LoginController@login',
);

app('Route')::middleware('api')->post(
    '/logout',
    'LoginController@logout',
);

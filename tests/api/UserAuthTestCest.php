<?php

use Illuminate\Support\Facades\Hash;

class UserAuthTestCest
{
    // tests
    public function testLoginIsSuccess(ApiTester $I)
    {
        $I->wantToTest('login is success');
        $I->sendPOST(
            '/auth/login',
            [
                'email' => $I->email,
                'password' => $I->password,
            ],
        );
        $I->seeResponseCodeIs(200);
        $I->seeResponseJsonMatchesJsonPath('$.access_token');
        $I->token = $I->grabDataFromResponseByJsonPath('$.access_token')[0];
    }

    public function testLoginIsFailed(ApiTester $I)
    {
        $I->wantToTest('login is failed');
        $I->sendPOST(
            '/auth/login',
            [
                'email' => $I->email,
                'password' => 'C0nv3n!@'
            ],
        );
        $I->seeResponseCodeIs(401);
    }
}

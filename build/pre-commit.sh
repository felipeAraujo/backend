#!/bin/sh

set -e

./vendor/bin/phpstan analyse --memory-limit=2G
vendor/bin/phpcs -p -s --standard=ruleset.xml app
vendor/bin/phpcs -p -s --standard=ruleset.xml tests
vendor/bin/phpcs -p -s --standard=ruleset.xml routes
